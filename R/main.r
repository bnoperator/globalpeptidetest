#' @import bnutil
#' @import reshape2
#' @import globaltest
#' @import dplyr
#' @export
shinyServerRun = function(input, output, session, context) {

  getFolderReactive = context$getRunFolder()
  getDataReactive = context$getData()
  getSettingsReactive = context$getFolder()

  output$body = renderUI({
    fluidPage(
      selectInput("group","Grouping Factor", choices = list() ),
      tableOutput("groupsum"),
      tableOutput("globalresult")
    )
  })

  observe({
    getFolder = getFolderReactive$value
    if (is.null(getFolder)) return()
    folder = getFolder()

    getData=getDataReactive$value
    if (is.null(getData)) return()

    bndata = getData()
    labelList = bndata$arrayLabels
    columnNameList = bndata$arrayColumnNames

    if(bndata$hasXAxis){
      labelList = c(labelList, bndata$xAxisLabel)
      columnNameList = c(columnNameList, bndata$xAxisColumnName)
    }
    if(bndata$hasColors){
      labelList = c(labelList, bndata$colorLabels)
      columnNameList = c(columnNameList, bndata$colorColumnNames)
    }
    names(columnNameList) = labelList
    updateSelectInput(session, "group", choices = columnNameList)

    if(bndata$hasColors){
      updateSelectInput(session, "group", selected = bndata$colorLabels[1])
    } else {
      updateSelectInput(session, "group", selected = columnNameList[1])
    }

    output$globalresult = renderTable({
        X = acast(bndata$data, colSeq~ rowSeq, value.var = "value")
        grp = acast(bndata$data, colSeq~ rowSeq, value.var = input$group)[,1]
        gres = gt(as.factor(grp), X)
        show(gres)
    })

    output$groupsum = renderTable({
        pep = bndata$data %>% filter(rowSeq == min(rowSeq))
        grp = as.factor(pep[[input$group]])
        result = data.frame(level = names(summary(grp)), count = summary(grp))

    })
  })
}
